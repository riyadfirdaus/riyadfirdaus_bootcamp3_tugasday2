import "./App.css";
import Stopwatch from "./components/Stopwatch";

function App() {
  return (
    <div className="App-container">
      <h1>Day 2 - Tugas 1</h1>
      <subtitle>Riyad Firdaus</subtitle>
      <Stopwatch />
    </div>
  );
}

export default App;
