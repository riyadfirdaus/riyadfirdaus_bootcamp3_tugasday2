import React from "react";
import style from "./Buttons.module.css";

const Button = (props) => {
  const resetButton = (
    <button className={`${style.btn} ${style.white}`} onClick={props.reset}>
      Reset
    </button>
  );
  const startButton = (
    <button className={`${style.btn} ${style.blue}`} onClick={props.start}>
      Start
    </button>
  );
  const stopButton = (
    <button className={`${style.btn} ${style.red}`} onClick={props.pause}>
      Stop
    </button>
  );

  return (
    <div className={style.container}>
      {resetButton}
      {startButton}
      {stopButton}
    </div>
  );
};

export default Button;
