import React, { useEffect, useState } from "react";
import style from "./Stopwatch.module.css";
import Counter from "./Counter";
import Buttons from "./Buttons";

const Stopwatch = () => {
  const [time, setTime] = useState(0);
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    let interval = null;
    if (isActive) {
      interval = setInterval(addTime, 10);
    } else {
      clearInterval(interval);
    }

    return () => {
      clearInterval(interval);
    };
  }, [isActive]);

  function addTime() {
    setTime((time) => time + 1);
  }
  function resetTime() {
    setIsActive(false);
    setTime(0);
  }

  function startTime() {
    setIsActive(true);
  }

  function pauseTime() {
    setIsActive(false);
  }

  return (
    <div className={style.container}>
      <Counter time={time} />
      <Buttons start={startTime} pause={pauseTime} reset={resetTime} />
    </div>
  );
};

export default Stopwatch;
