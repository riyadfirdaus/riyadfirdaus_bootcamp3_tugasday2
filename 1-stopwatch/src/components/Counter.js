import React from "react";
import style from "./Counter.module.css";

const Counter = ({ time }) => {
  function formatNumber(n) {
    return n.toLocaleString("en-US", {
      minimumIntegerDigits: 2,
      useGrouping: false,
    });
  }

  const hours = formatNumber(Math.floor(time / 360000));
  const minutes = formatNumber(Math.floor((time % 360000) / 6000));
  const seconds = formatNumber(Math.floor((time % 6000) / 100));

  return (
    <div className={style.counter}>
      <p>
        {hours} : {minutes} : {seconds}
      </p>
      <p>Jam : Menit : Detik</p>
    </div>
  );
};

export default Counter;
